﻿using UnityEngine;
using System.Collections;
using Rewired;


public class UnityButtonProblem : MonoBehaviour {

	public int playerId = 0; // The Rewired player id of this character

	private Player player; // The Rewired Player

	private string lbl;

	[System.NonSerialized] // Don't serialize this so the value is lost on an editor script recompile.
	private bool initialized;

	private void Initialize() {
	    // Get the Rewired Player object for this player.
	    player = ReInput.players.GetPlayer(playerId);
	    
	    initialized = true;
	}

	void Update() {
	    if(!ReInput.isReady) return; // Exit if Rewired isn't ready. This would only happen during a script recompile in the editor.
	    if(!initialized) Initialize(); // Reinitialize after a recompile in the editor

	}

	public void OnGUI()
	{
		lbl = "RawInput: ";
		for( int i = 0; i < 4; i++)
			lbl += player.GetButton("b" + i.ToString()).ToString() + " ";  // btnState[i].ToString();

		lbl += "\nUnity Input: ";

		//lbl += player.GetButton("b" + i.ToString())
		for( int i = 0; i < 4; i++)
			lbl += Input.GetButton("Joy1Button" + i.ToString()).ToString() + " ";

		GUILayout.Label(lbl);
	}

}
